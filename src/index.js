import React from 'react';
import ReactDOM from 'react-dom';
import { IMAGE_WIDTH, IMAGE_HEIGHT, PIXEL_COUNT } from './constants';
import { Canvas } from './components/canvas';
import generateScanData from './algorithms/scan';
import generateVoidData from './algorithms/void';
import generateVoronoiData from './algorithms/voronoi';
import './index.css';

class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            pixelData: new Uint8ClampedArray(PIXEL_COUNT * 4).fill(255),
        };
    }

    draw(context, pixelData) {
        const imageData = new ImageData(pixelData, IMAGE_WIDTH);
        context.putImageData(imageData, 0, 0);
    }

    generateScan = () => {
        this.setState({
            pixelData: generateScanData(),
        });
    }

    generateVoid = () => {
        this.setState({
            pixelData: generateVoidData(),
        });
    }

    generateVoronoi = () => {
        this.setState({
            pixelData: generateVoronoiData(),
        });
    }

    render() {
        return (
            <div>
                <div>
                    <button onClick={this.generateScan}>Generate Scan</button>
                    <button onClick={this.generateVoid}>Generate Void</button>
                    <button onClick={this.generateVoronoi}>Generate Voronoi</button>
                </div>
                <Canvas width={IMAGE_WIDTH} height={IMAGE_HEIGHT} draw={this.draw} pixelData={this.state.pixelData} />
            </div>
        );
    }
}

ReactDOM.render(
    <App />,
    document.getElementById('root')
);
