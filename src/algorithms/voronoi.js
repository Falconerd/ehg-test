import { PIXEL_COUNT, IMAGE_WIDTH, IMAGE_HEIGHT } from '../constants';
import { vectorDistance } from '../util';

export default function generateVoronoiData() {
    const newPixelData = new Uint8ClampedArray(PIXEL_COUNT * 4);
    let pixelObjectData = new Array(PIXEL_COUNT);
    let i = 0;
    for (let r = 255; r > 0; r -= 8) {
        for (let g = 255; g > 0; g -= 8) {
            for (let b = 255; b > 0; b -= 8) {
                pixelObjectData[i] = { r, g, b };
                i++;
            }
        }
    }

    /* Sort by brightness */
    pixelObjectData.sort((a, b) => {
        return (b.r + b.g + b.b) - (a.r + a.g + a.b);
    });

    const points = new Array(8);
    const voronoiData = new Array(PIXEL_COUNT);
    for (let i = 0; i < 8; ++i) {
        points[i] = {
            x: Math.floor(Math.random() * IMAGE_WIDTH) + 1,
            y: Math.floor(Math.random() * IMAGE_HEIGHT) + 1,
        };
    }

    for (let i = 0; i < PIXEL_COUNT; ++i) {
        let closestIndex = 0;
        let closestDist = IMAGE_HEIGHT + 1;
        for (let j = 0; j < 8; ++j) {
            const x = i % IMAGE_WIDTH;
            const y = i / IMAGE_WIDTH - i / IMAGE_WIDTH % 1;
            const dist = vectorDistance(points[j], { x, y });
            if (dist < closestDist) {
                closestDist = dist;
                closestIndex = j;
            }
        }
        voronoiData[i] = closestIndex;
    }

    let head = 0;
    let tail = PIXEL_COUNT - 1;
    for (let i = 0; i < PIXEL_COUNT; ++i) {
        let colour;
        if (0 === (voronoiData[i] % 2)) {
            colour = pixelObjectData[tail--];
        } else {
            colour = pixelObjectData[head++];
        }
        newPixelData[i * 4 + 0] = colour.r;
        newPixelData[i * 4 + 1] = colour.g;
        newPixelData[i * 4 + 2] = colour.b;
        newPixelData[i * 4 + 3] = 255;
    }
    return newPixelData;
}
