import { PIXEL_COUNT, IMAGE_WIDTH, IMAGE_HEIGHT } from '../constants';
import { vectorDistance } from '../util';

export default function generateVoidData() {
    const newPixelData = new Uint8ClampedArray(PIXEL_COUNT * 4);
    const pixelObjectData = new Array(PIXEL_COUNT);
    let i = 0;
    for (let r = 255; r > 0; r -= 8) {
        for (let g = 255; g > 0; g -= 8) {
            for (let b = 255; b > 0; b -= 8) {
                pixelObjectData[i] = { r, g, b };
                i++;
            }
        }
    }

    /* Sort by brightness */
    pixelObjectData.sort((a, b) => {
        return (b.r + b.g + b.b) - (a.r + a.g + a.b);
    });

    /* Create a set of points which correspond to pixel coordinates
     * and sort them by how close to the centre they are. */
    var points = new Array(PIXEL_COUNT);
    for (let i = 0; i < PIXEL_COUNT; ++i) {
        const x = i % IMAGE_WIDTH;
        const y = i / IMAGE_WIDTH - i / IMAGE_WIDTH % 1;
        points[i] = { x, y };
    }

    const centre = { x: IMAGE_WIDTH / 2, y: IMAGE_HEIGHT / 2 };

    points.sort((a, b) => {
        return vectorDistance(b, centre) - vectorDistance(a, centre);
    });

    for (let i = 0; i < PIXEL_COUNT; ++i) {
        const x = points[i].x;
        const y = points[i].y;
        const index = y * IMAGE_WIDTH + x;
        newPixelData[index * 4 + 0] = pixelObjectData[i].r;
        newPixelData[index * 4 + 1] = pixelObjectData[i].g;
        newPixelData[index * 4 + 2] = pixelObjectData[i].b;
        newPixelData[index * 4 + 3] = 255;
    }
    return newPixelData;
}
