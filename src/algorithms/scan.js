import { PIXEL_COUNT } from '../constants';

export default function generateScanData() {
    const newPixelData = new Uint8ClampedArray(PIXEL_COUNT * 4);
    let i = 0;
    for (let r = 255; r > 0; r -= 8) {
        for (let g = 255; g > 0; g -= 8) {
            for (let b = 255; b > 0; b -= 8) {
                newPixelData[i * 4 + 0] = r;
                newPixelData[i * 4 + 1] = g;
                newPixelData[i * 4 + 2] = b;
                newPixelData[i * 4 + 3] = 255;
                i++;
            }
        }
    }
    return newPixelData;
}
