import React, { useRef, useEffect } from 'react';

export const Canvas = props => {
    const { draw, pixelData, ...rest } = props;
    const _ref = useRef(null);

    useEffect(() => {
        const canvas = _ref.current;
        const context = canvas.getContext('2d');

        draw(context, pixelData);
    }, [draw, pixelData]);

    return <canvas ref={_ref} {...rest} />
};
