export const IMAGE_WIDTH = 128;
export const IMAGE_HEIGHT = 256;
export const PIXEL_COUNT = IMAGE_WIDTH * IMAGE_HEIGHT;
