import { PIXEL_COUNT } from '../src/constants';
import generateVoidData from '../src/algorithms/void';

describe("Generate a circular void shape", () => {
    test("It should have no repeating colours", () => {
        const voidData = generateVoidData();
        let doubles = 0;
        let found = {};

        for (let i = 0; i < PIXEL_COUNT * 4; i += 4) {
            let r = voidData[i + 0];
            let g = voidData[i + 1];
            let b = voidData[i + 2];
            if (true === found[`${r}${g}${b}`]) {
                doubles++;
            }
            found[`${r}${g}${b}`] = true;
        }

        expect(doubles).toEqual(0);
    });
    test("It should not contain black", () => {
        const voidData = generateVoidData();
        for (let i = 0; i < PIXEL_COUNT * 4; i += 4) {
            let r = voidData[i + 0];
            let g = voidData[i + 1];
            let b = voidData[i + 2];
            expect(0 === r && r === g && r === b).not.toBe(true);
        }
    });
});
