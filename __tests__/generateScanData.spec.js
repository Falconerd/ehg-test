import { PIXEL_COUNT } from '../src/constants';
import generateScanData from '../src/algorithms/scan';

describe("Generate a simple colour scan", () => {
    test("It should have no repeating colours", () => {
        const scanData = generateScanData();
        let doubles = 0;
        let found = {};

        for (let i = 0; i < PIXEL_COUNT * 4; i += 4) {
            let r = scanData[i + 0];
            let g = scanData[i + 1];
            let b = scanData[i + 2];
            if (true === found[`${r}${g}${b}`]) {
                doubles++;
            }
            found[`${r}${g}${b}`] = true;
        }

        expect(doubles).toEqual(0);
    });
    test("It should not contain black", () => {
        const scanData = generateScanData();
        for (let i = 0; i < PIXEL_COUNT * 4; i += 4) {
            let r = scanData[i + 0];
            let g = scanData[i + 1];
            let b = scanData[i + 2];
            expect(0 === r && r === g && r === b).not.toBe(true);
        }
    });
});
