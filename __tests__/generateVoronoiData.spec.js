import { PIXEL_COUNT } from '../src/constants';
import generateVoronoiData from '../src/algorithms/voronoi';

describe("Generate a Voronoi diagram", () => {
    test("It should have no repeating colours", () => {
        const voronoiData = generateVoronoiData();
        let doubles = 0;
        let found = {};

        for (let i = 0; i < PIXEL_COUNT * 4; i += 4) {
            let r = voronoiData[i + 0];
            let g = voronoiData[i + 1];
            let b = voronoiData[i + 2];
            if (true === found[`${r}${g}${b}`]) {
                doubles++;
            }
            found[`${r}${g}${b}`] = true;
        }

        expect(doubles).toEqual(0);
    });
    test("It should not contain black", () => {
        const voronoiData = generateVoronoiData();
        for (let i = 0; i < PIXEL_COUNT * 4; i += 4) {
            let r = voronoiData[i + 0];
            let g = voronoiData[i + 1];
            let b = voronoiData[i + 2];
            expect(0 === r && r === g && r === b).not.toBe(true);
        }
    });
});
